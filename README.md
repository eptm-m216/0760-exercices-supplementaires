# 0760 Exercices supplémentaires

## Avertissement

**Ces exercices supplémentaires sont une reprise du [tutoriel git de UrbanInstitute](https://github.com/UrbanInstitute/git-tutorial/tree/master).**

## Cloner un dépôt

(Je vais utiliser beaucoup de terminologie spécifique à git ici, si vous avez besoin d'un rafraîchissement, vous pouvez consulter [l'aperçu](overview.md) ou le [glossaire](glossary.md))

1. Pour cloner un dépôt sur votre ordinateur local, allez d'abord sur la page d'accueil du dépôt (pour cette leçon, clonons le [dépôt git-tutorial](https://gitlab.com/eptm-m216/0760-exercices-supplementaires.git)).

2. Sur le côté droit de la page, trouvez le bouton vert CODE.

3. Copiez l'URL sous l'onglet HTTPS. Pour ce dépôt, c'est "https://gitlab.com/eptm-m216/0760-exercices-supplementaires.git".
4. Dans une nouvelle fenêtre Terminal, exécutez :
	```bash
	git clone l_url
	```
	Remplacez `l_url` par l'URL que vous venez de copier.

5. Dans le Terminal, pour entrer dans le dossier que vous venez de cloner (qui est automatiquement nommé "git-tutorial"), exécutez ```cd git-tutorial```.

## Faites un peu de travail !

1. Créez, supprimez ou modifiez quelques fichiers ou dossiers. Si vous trouvez des fautes de frappe, etc., dans ces tutoriels, n'hésitez pas à les modifier, mais j'ai également créé un dossier appelé [workspace](workspace) où vous pouvez créer et modifier des fichiers. Vous pouvez par exemple créer une feuille de style CSS, ou écrire une liste d'instructions pour réaliser le meilleur sandwich à la dinde de tous les temps. Faites ce que vous voulez.

2. Maintenant, depuis le dossier `git-tutorial`, exécutez :
	```bash
	git status
	```
	Vous devriez voir quelque chose de similaire à ceci :
	![git status](images/git_status.png)
	
	Cela vous donne beaucoup d'informations, que je vais expliquer en détail.
	- `On branch master` signifie que je travaille sur la branche principale, appelée master. Voir ci-dessous pour les instructions sur la création de nouvelles branches.
	- `Your branch is up-to-date with 'origin/master'` signifie qu'il n'y a actuellement aucun changement prêt à être poussé vers le dépôt distant.
	- La section `Changes not staged for commit` liste trois fichiers qui ont été modifiés (README.md, overview.md et working.md) depuis mon dernier push, mais ces changements n'ont pas encore été enregistrés dans un commit.
	- La section `Untracked files` liste les fichiers ou dossiers qui n'existaient pas lors de mon dernier push, et qui ont été créés depuis.

3. Avant de valider les changements, vous devez *les ajouter* au commit (un peu comme les mettre de côté). Cette étape fait partie du flux de travail pour vous permettre de faire un tas de travail, puis de le séparer en plusieurs commits. Par exemple, si vous travaillez pendant quelques heures sur un graphique pour le rendre conforme à la norme pour les daltoniens, vous pouvez ajouter tous les fichiers du dossier `css` à un commit, puis le pousser avec le message de commit "mise à jour des feuilles de style pour répondre aux exigences daltoniennes". Ensuite, vous pouvez ajouter le fichier `README.md` ou d'autres fichiers de documentation à un nouveau commit, et le pousser avec le message "La documentation décrit désormais la conformité au daltonisme et les ressources". Si cette terminologie semble trop complexe, continuez à lire, et espérons que tout deviendra clair.

	Vous avez plusieurs options pour ajouter des fichiers à un commit.

	```bash
	git add -A
	```
	ajoutera tous les fichiers (les trois fichiers .md ainsi que tout ce qui est dans le dossier `images`, dans ce cas) au commit. Le `-A` signifie "tous".

	*Petit conseil : si vous avez des fichiers que vous ne voulez jamais ajouter à un commit, vous pouvez créer un fichier caché spécial dans le dépôt appelé `.gitignore`. Dans le [git-tutorial .gitignore](.gitignore), je demande à git de ne jamais ajouter le fichier OS X appelé [.DS_Store](http://en.wikipedia.org/wiki/.DS_Store) qui vit dans chaque dossier et stocke des informations comme les positions des icônes de fichiers, ainsi que tout fichier qui se termine par .config (qui pourrait contenir des informations comme des mots de passe que je ne veux jamais pousser sur github). Avec un bon .gitignore, vous pouvez en toute sécurité exécuter `git add -A` sans vous soucier d'ajouter accidentellement des fichiers qui ne devraient pas être validés.*

	```bash
	git add *.md
	```
	ajoutera tous les fichiers se terminant par ".md" au commit. Dans cet exemple, cela ajoutera README.md, overview.md et working.md.

	```bash
	git add README.md
	```
	ajoutera uniquement un fichier, README.md, au commit.

	Une fois que vous avez ajouté tous les fichiers .md, vous pouvez exécuter :
	```bash
	git status
	```
	encore, et vous verrez que vos changements sont maintenant prêts à être validés, super !
	![git status2](images/git_status2.png)

	En général, si vous n'êtes pas sûr de ce qu'il faut faire ensuite, exécuter `git status` fréquemment vous donnera une idée de ce que vous devez faire.

4. Ensuite, vous devez valider les fichiers. Tous les commits doivent être décrits par un *message de commit* qui décrit les changements que vous avez effectués dans leur ensemble. Voici un exemple simple d'un commit sur git-tutorial, vu sur github.com :
![simple commit](images/simple_commit.png)
Notez quelques éléments. Le message de commit est "Example typo commit", ce qui nous indique que j'ai fait ce commit pour vous montrer un exemple simple de commit visant à corriger une faute de frappe. README.md a été modifié, et github met en surbrillance les lignes avec des suppressions en rouge (à gauche), avec les caractères supprimés en rouge foncé. Les lignes/ caractères ajoutés sont montrés en vert à droite.

La page pour ce commit est [ici](https://gitlab.com/eptm-m216/0760-exercices-supplementaires/-/commit/a7fc4bef6dde493c62fd60229ed2ebc9ef20ca14), ou vous pouvez cliquer sur ![commits link](images/commits_link.png) en haut de la page principale du dépôt (le nombre sera bien sûr différent).

Pour faire un commit, exécutez la commande suivante :
	```bash
	git commit -m "Voici où vous mettez votre message de commit"
	```

Maintenant tous ces changements que vous avez effectués sont stockés dans un seul commit.

Parfois, vous pouvez faire beaucoup de travail pour un seul commit, ce qui est difficile à décrire dans un message de commit bref. Dans ce cas, vous pouvez rédiger un message de commit plus long et plus organisé en exécutant simplement :
	```bash
	git commit
	```
	(Sans le flag `-m`) Cela ouvrira votre éditeur de texte (consultez le [tutoriel d'installation](setup.md) pour des instructions sur l'utilisation de Sublime en tant qu'éditeur par défaut de git), où vous pourrez rédiger un message de commit plus long. Sauvegardez et fermez le fichier, et votre message sera enregistré. *Note : Les messages de commit longs peuvent être écrits en utilisant le "markdown Github flavor", qui est une manière de faire facilement des choses comme créer des listes, mettre en gras ou en italique, ajouter des titres, écrire du code dans ces petites boîtes grises qu'on trouve partout dans ce tutoriel, etc. Les fichiers README et d'autres fichiers comme celui-ci sont écrits en markdown, en enregistrant un fichier texte avec l'extension ".md". Vous pouvez également utiliser [de nombreuses balises HTML](https://github.com/github/markup/tree/master#html-sanitization) dans un document .md. Vous pouvez en savoir plus sur [github flavored markdown ici](https://help.github.com/articles/github-flavored-markdown/)*

[Certains](http://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message) estiment que vous devriez toujours écrire des messages de commit longs et détaillés, bien que souvent ce ne soit pas nécessaire. Les commits longs sont super quand vous avez fait "beaucoup de petites choses", comme [cet exemple](https://github.com/UrbanInstitute/git-tutorial/commit/7e1f885187e93627fa0f763adb75dd7bc04fb43a), ou un [exemple plus réaliste](https://github.com/UI-Research/UI-Graphics/commit/860f33932b0ff7225e7f7900bf3c6db7a70e6b0f) d'un travail que j'ai fait aujourd'hui sur un autre projet.

5. Ok, vous avez validé vos changements (vous pouvez vérifier avec un autre `git status`), maintenant vous devez les pousser sur github. D'abord, exécutez :
	```bash
	git pull
	```
	Cela tirera les modifications du dépôt github distant que d'autres utilisateurs ont effectuées depuis la dernière fois que vous avez poussé ou tiré. Il est bon de noter que presque tout ce que vous avez fait jusqu'à présent dans ce tutoriel (`add`, `status`, `commit`) peut être fait sans connexion Internet. C'est uniquement lorsque vous poussez ou tirez que vous vous connectez au dépôt github distant, via Internet. Parfois, les changements que vous essayez de tirer peuvent contredire les changements que vous avez effectués, mais pas encore poussés. Si c'est le cas, consultez la section [résolution d'un conflit de fusion](#résolution-d-un-conflit-de-fusion) ci-dessous.

6. Si le tirage s'est bien passé, vous pouvez maintenant exécuter :
	```bash
	git push
	```
	et vos changements seront enregistrés sur github ! Hourra !

7. Il est recommandé de faire beaucoup de commits fréquents, avec des messages descriptifs, pour plusieurs raisons :
	- Git facilite le retour en arrière d'un projet à l'état dans lequel il se trouvait lors d'un certain commit. Si vous faites beaucoup de petits commits, cela vous permet de revenir facilement sur une erreur ou une modification, sans effacer également un travail que vous souhaitez conserver.

	- En examinant un historique de commits bien tenu, il est facile de voir qui a travaillé sur un projet, quand, ce qu'ils ont fait, et pourquoi ils l'ont